
package hello;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.core.SpringVersion;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    CustomerRepository repository;
    @Autowired
    CustomerObjectRepository customerObjectRepository;
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... strings) throws Exception {
        // save a couple of customers
    	CustomerObject obj1 = new CustomerObject(3);
       	customerObjectRepository.save(obj1);
        repository.save(new Customer("Jack", "Bauer", obj1));
        
        CustomerObject obj2 = new CustomerObject(5);
    	customerObjectRepository.save(obj2);
        repository.save(new Customer("Chloe", "O'Brian", obj2));
        
        CustomerObject obj3 = new CustomerObject(4);
    	customerObjectRepository.save(obj3);
        repository.save(new Customer("Kim", "Bauer", obj3));
        
        CustomerObject obj4 = new CustomerObject(1);
    	customerObjectRepository.save(obj4);
        repository.save(new Customer("David", "Palmer", obj4));
        
        CustomerObject obj5 = new CustomerObject(2);
    	customerObjectRepository.save(obj5);
        repository.save(new Customer("Michelle", "Dessler", obj5));
        
        String s = String.format("%d",obj5.getObjectNum());
    	System.out.println(s);
        
    	System.out.println("version: " + SpringVersion.getVersion());
        // fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();
        
        // fetch an individual customer by ID
        Customer customer = repository.findOne(1L);
        System.out.println(customer.getCustomerObject().getObjectNum());
        System.out.println("Customer found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(customer);
        System.out.println();
        
        
        // fetch customers by last name
        
        System.out.println("Customer found with findByLastName('Bauer'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : repository.findByLastName("Bauer")) {
            System.out.println(bauer);
            
        }
        System.out.println("--------------------------------------------");
        System.out.println("Customer in sorted format:");
        System.out.println("--------------------------------------------");
        List<CustomerObject> sorted = repository.findCustomerObjectByCustomerObject_ObjectNum(2);
        
        //overWriteRecord();
        for(CustomerObject palmer : sorted){
        	System.out.println(palmer);
        	
        }
        
        
        
    }
    @Transactional
    public void overWriteRecord(){
    	
    }

}
