package hello;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="customers", schema="customerSchema")
public class Customer {
	
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private String firstName;
    private String lastName;
    
    @OneToOne(mappedBy="customer")
    private CustomerObject customerObject;

    public CustomerObject getCustomerObject() {
		return customerObject;
	}

	public void setCustomerObject(CustomerObject customerObject) {
		this.customerObject = customerObject;
	}

	protected Customer() {}
    
	public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public Customer(String firstName, String lastName, CustomerObject customerObject) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.customerObject = customerObject;
    }

    @Override
    public String toString() {
    	CustomerObject c = this.getCustomerObject();
    	return String.format(
        		"Customer[id=%d, firstName='%s', lastName='%s', objectNum=%d]"
        		,id, firstName, lastName, c.getObjectNum());
    }

}

