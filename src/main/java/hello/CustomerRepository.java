package hello;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
    @Transactional
    List<Customer> findByLastName(String lastName);
    List<Customer> findAllByOrderByFirstNameDesc();
    List<CustomerObject> findCustomerObjectByCustomerObject_ObjectNum(int objectNum);
}
