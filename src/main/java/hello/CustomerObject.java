package hello;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="customerObject", schema="customerSchema")
public class CustomerObject {
	@Id
    @Column(name = "obj_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long objId;
	
	private int objectNum;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private Customer customer;
	
	protected CustomerObject() {}

	public CustomerObject(int objectNum) {
		super();
		this.objectNum = objectNum;
	}



	public long getObjId() {
		return objId;
	}

	public void setObjId(long objId) {
		this.objId = objId;
	}

	public int getObjectNum() {
		return objectNum;
	}

	public void setObjectNum(int objectNum) {
		this.objectNum = objectNum;
	}


}
